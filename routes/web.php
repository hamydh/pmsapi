<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::get('/searchprofile', 'ProfileController@search')->name('searchprofile')->middleware('auth');
Route::post('/createprofile', 'ProfileController@store')->name('profile')->middleware('auth');
Route::get('/profile', 'ProfileController@index')->name('profile')->middleware('auth');
Route::get('/profile/{id}', 'ProfileController@show')->name('showprofile')->middleware('auth');
Route::PUT('updateprofile/{id}', 'ProfileController@update')->middleware('auth');
Route::post('loginapi', 'ProfileController@login');
Route::get('logoutapi', 'ProfileController@logout');
Route::get('currentuser', 'ProfileController@currentuser');
Route::delete('/deleteprofile/{id}', 'ProfileController@destroy')->name('deleteprofile')->middleware('auth');

