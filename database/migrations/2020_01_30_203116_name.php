<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class Name extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
           Schema::create('name', function (Blueprint $table) {
            $table->bigIncrements('name_id');
            $table->string('first_name')->nullable();
            $table->string('last_name')->nullable();
            $table->string('language')->nullable();
            $table->string('title')->nullable();
            $table->string('address')->nullable();
            $table->string('city')->nullable();
            $table->string('postalcode')->nullable();
            $table->string('country')->nullable();
            $table->string('state')->nullable();
            $table->string('salution')->nullable();
            $table->string('vip')->nullable();
            $table->string('nationality')->nullable();
            $table->string('currency')->nullable();
            $table->string('mailaction')->nullable();
            $table->boolean('mailinglist_yn')->default('0');
            $table->string('keyword')->nullable();
            $table->string('gender')->nullable();
            $table->longText('notes')->nullable();
            $table->date('dob')->nullable();
            $table->string('idnumber')->nullable();
            $table->date('expirydate')->nullable();
            $table->string('mobile')->nullable();
            $table->string('email')->nullable();
            $table->boolean('active')->default('0');
            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
       Schema::dropIfExists('name');
    }
}
