Commands to run:
-----------------------------------
1.install composer from below link
->https://getcomposer.org/doc/00-intro.md
2.create mysql database with the name 'pms'
3.change directory to pmsapi run the below commands
-> composer install
-> php artisan migrate
-> php artisan key:generate
-> php artisan serve
4. when you have run the last command it will serve service in localhost:8000
--------------------------------------------------------------------------------------
Api details
--------------
->To login to api post 'email' and 'password' fields to loginapi url (POST)
->To get all profile use url https://localhost:8000/profile (GET)
->To get one profile use url https://localhost:8000/profile/id (GET)
->To delete profile use url https://localhost:8000/deleteprofile/id (DELETE)
->To update profile use url https://localhost:8000/updateprofile/id (PUT)
->To search profile use url https://localhost:8000/searchprofile/params (GET)