<?php

namespace App\Http\Controllers;
use App\profile;
use Auth;
use Illuminate\Http\Request;

class ProfileController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
 
     return profile::all();

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        
       $profile = profile::create($request->all());
        return response()->json($profile, 201);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
       $profile = profile::find($id);

        if (is_null($profile)) {

             return response()->json('Profile doest not exist');
        }
   
         return response()->json($profile);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $profile = profile::find($id);
        $profile->update($request->all());
    
         return response()->json($profile, 200);
       
   
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $profile = profile::findOrFail($id);

        $profile->delete();
       return response()->json(null, 204);
        
    } 

    public function search(Request $request)
    {
       $profile = profile::where('first_name', 'like', '%'.$request->first_name)
                         ->Where('last_name', 'like', '%'.$request->last_name)
                         ->get();

      return response()->json($profile, 200);
       
        
    }

     public function login(Request $request)
     {
       $loginDetails = $request->only('email','password');

       if(Auth::attempt($loginDetails)){

           return response()->json(['currentuser'=> Auth::user(),'message' => 'login successful', 'code' => 200]);
       }else{
           return response()->json(['message' => 'wrong login details', 'code' => 400]);
       }
    } 
 
    public function logout(){
    Auth::logout();
    return response()->json(['message' => 'Logout', 'code' => 400]);
   }

   public function currentuser(){
    
    return response()->json(['currentuser' => Auth::user()]);
   }

}
