<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class profile extends Model
{
         public $fillable = [
        'name_id', 'first_name', 'last_name',
        'language', 'title', 'address',
        'city', 'postalcode', 'country',
        'state', 'salution', 'vip',
        'nationality', 'currency', 'mailaction',
        'mailinglist_yn', 'keyword', 'gender',
        'notes', 'dob', 'idnumber',
        'expirydate', 'mobile', 'email',
        'active',
    ];
}
